package ru.shumov.tm;

import java.text.ParseException;

public class Application {
    public static void main(String[] args) throws ParseException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.start();
    }
}
