package ru.shumov.tm;
import ru.shumov.tm.repository.ProjectRepository;
import ru.shumov.tm.repository.TaskRepository;
import ru.shumov.tm.service.Data;
import ru.shumov.tm.service.ProjectService;
import ru.shumov.tm.service.TaskService;

import java.text.ParseException;

import static ru.shumov.tm.Commands.*;
import static ru.shumov.tm.Commands.EXIT;

public class Bootstrap {
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private Data data;

    public void init() {
        data = new Data();
        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository, taskService, data);
        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository, projectService, data);
    }

    public void start() throws ParseException {
        Help help = new Help();
        boolean work = true;
        data.outPutString(WELCOME);
        data.outPutString(PROJECT_ID);
        while (work) {
            String commandName = data.scanner();
            switch (commandName) {
                case HELP -> help.help();
                case PROJECT_CREATE -> projectService.create();
                case PROJECT_CLEAR -> projectService.clear();
                case PROJECT_REMOVE -> projectService.remove();
                case GET_PROJECT -> projectService.getOne();
                case PROJECT_LIST -> projectService.getList();
                case PROJECT_UPDATE -> projectService.update();
                case TASK_CREATE -> taskService.create();
                case TASK_CLEAR -> taskService.clear();
                case TASK_REMOVE -> taskService.remove();
                case GET_TASK -> taskService.getOne();
                case TASK_LIST -> taskService.getList();
                case TASK_UPDATE -> taskService.update();
                case EXIT -> work = false;
            }
        }
    }
}
