package ru.shumov.tm.repository;
import ru.shumov.tm.entity.Project;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ProjectRepository {

    private Map<String, Project> projects = new HashMap<>();

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findOne(String id) {
        return projects.get(id);
    }

    public void persist(String id, Project project) {
        if(!projects.containsKey(id)){
            projects.put(id, project);
        }
    }

    public void merge(Project project) {
        projects.put(project.getId(), project);
    }

    public void remove(String id) {
        projects.remove(id);
    }

    public void removeAll() {
        projects.clear();
    }
}
