package ru.shumov.tm.repository;
import ru.shumov.tm.entity.Task;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TaskRepository {

    private Map<String, Task> tasks = new HashMap<>();

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findOne(String id) {
        return tasks.get(id);
    }

    public void persist(Task task) {
        if(tasks.containsKey(task.getId())){
            tasks.put(task.getId(), task);
        }
    }

    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(String id) {
        tasks.remove(id);
    }

    public void removeAll() {
        tasks.clear();
    }
}
