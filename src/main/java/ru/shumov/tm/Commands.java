package ru.shumov.tm;

public class Commands {

    public final static String HELP = "help";
    public final static String HELP_HELP = "help : Вывод доступных команд.";
    public final static String PROJECT_CREATE = "project create";
    public final static String HELP_PROJECT_CREATE = "project create: Создание нового проекта.";
    public final static String PROJECT_CLEAR = "project clear";
    public final static String HELP_PROJECT_CLEAR = "project clear: Удаление всех проектов.";
    public final static String PROJECT_REMOVE = "project remove";
    public final static String HELP_PROJECT_REMOVE = "project remove: Выборочное удаление проектов.";
    public final static String PROJECT_LIST = "project list";
    public final static String HELP_PROJECT_LIST = "project list: Вывод всех проектов.";
    public final static String GET_PROJECT = "get project";
    public final static String HELP_GET_PROJECT = "get project: Вывод конкретного проекта.";
    public final static String TASK_CREATE = "task create";
    public final static String HELP_TASK_CREATE = "task create: Создание нового задания.";
    public final static String TASK_CLEAR = "task clear";
    public final static String HELP_TASK_CLEAR = "task clear: Удаление всех задач.";
    public final static String TASK_REMOVE = "task remove";
    public final static String HELP_TASK_REMOVE = "task remove: Выборочное удаление задач.";
    public final static String TASK_LIST = "task list";
    public final static String HELP_TASK_LIST = "task list: Вывод всех задач.";
    public final static String GET_TASK = "get task";
    public final static String HELP_GET_TASK = "get task: вывод конкретной задачи.";
    public final static String EXIT = "exit";
    public final static String PROJECT_UPDATE = "project update";
    public final static String HELP_PROJECT_UPDATE = "project update: Изменение параметров проекта.";
    public final static String TASK_UPDATE = "task update";
    public final static String HELP_TASK_UPDATE = "task update: Изменения параметров задачи.";
    public final static String HELP_EXIT = "exit: Остановка программы.";

    public final static String WELCOME = "       Добро Пожаловать в Task Manager";
    public final static String PROJECT_ID = "...ID Проекта Можно Найти в Списке Проектов...";

    public final static String DONE = "Done";
    public final static String YES_NO = "yes/no";
    public final static String YES = "yes";
    public final static String NO = "no";

    public final static String ENTER_PROJECT_NAME = "Введите имя проекта:";
    public final static String ENTER_PROJECT_ID_FOR_TASKS = "Чтобы добавить задание к проекту введите id проекта:";
    public final static String ENTER_PROJECT_ID_FOR_REMOVING = "Введите ID Проекта который вы хотите удалить:";
    public final static String ENTER_PROJECT_ID_FOR_SHOWING_TASKS = "Введите id проекта задачи которого нужно вывести:";
    public final static String ENTER_PROJECT_ID = "Введите id проекта который хотите вывести:";
    public final static String ENTER_DEADLINE_OF_PROJECT = "Введите дату окончания работы над проектом:";
    public final static String ENTER_START_DATE_OF_PROJECT = "Введите дату начала работы над проектом:";
    public final static String ENTER_DESCRIPTION_OF_PROJECT = "Введите описание проекта:";
    public final static String ENTER_ID_OF_PROJECT_FOR_SHOWING = "Введите id проекта который хотите изменить.";
    public final static String ENTER_TASK_NAME = "Введите назввание задачи:";
    public final static String ENTER_TASK_ID_FOR_REMOVING = "Введите id задачи которую вы хотите удалить:";
    public final static String ENTER_DEADLINE_OF_TASK = "Введите дату окончания работы над заданием:";
    public final static String ENTER_START_DATE_OF_TASK = "Введите дату начала работы над заданием:";
    public final static String ENTER_DESCRIPTION_OF_TASK = "Введите описание задания:";
    public final static String ENTER_ID_OF_TASK_FOR_SHOWING = "Введите id задания который хотите изменить.";

    public final static String PROJECT_DOES_NOT_EXIST = "Такого проекта нет.";
    public final static String PROJECTS_DO_NOT_EXIST = "Проектов нет";
    public final static String ALL_PROJECTS_WILL_BE_CLEARED = "Все проекты будут удалены, Вы уверны?";

    public final static String SHOW_ALL_PROJECTS = "Вывести все проекты?";
    public final static String ALL_TASKS_WILL_BE_CLEARED = "Все задачи будут удалены, Вы уверны?";
    public final static String TASKS_DO_NOT_EXIST = "Задач нет.";
    public final static String SHOW_ALL_TASKS = "Вывести все задачи?";
}
