package ru.shumov.tm.service;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.util.Scanner;

public class Data {

    Scanner scr = new Scanner(System.in);

    public String scanner() {
        String string = scr.nextLine();
        if(string == null || string.isEmpty()) {

        }
        return string;
    }

    public void outPutString(String string){
        System.out.println(string);
    }

    public void outPutProject(Project project){
        System.out.println(project);
    }

    public void outPutTask(Task task) {
        System.out.println(task);
    }
}
