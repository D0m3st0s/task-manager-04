package ru.shumov.tm.service;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.repository.ProjectRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class ProjectService {
    private ProjectRepository projectRepository;
    private TaskService taskService;
    private Data data;

    public ProjectService(ProjectRepository projectRepository, TaskService taskService, Data data) {
        this.projectRepository = projectRepository;
        this.taskService = taskService;
        this.data = data;
    }

    public void create() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        data.outPutString(Commands.ENTER_PROJECT_NAME);
        String name = data.scanner();

        Project project = new Project();
        String id = UUID.randomUUID().toString();
        data.outPutString(Commands.ENTER_START_DATE_OF_PROJECT);
        String startDateS = data.scanner();
        Date StartDateD = format.parse(startDateS);
        data.outPutString(Commands.ENTER_DEADLINE_OF_PROJECT);
        String endDateS = data.scanner();
        Date endDateD = format.parse(endDateS);
        data.outPutString(Commands.ENTER_DESCRIPTION_OF_PROJECT);
        String description = data.scanner();

        project.setDescription(description);
        project.setName(name);
        project.setStartDate(StartDateD);
        project.setEndDate(endDateD);
        project.setId(id);
        projectRepository.persist(id, project);
        data.outPutString(Commands.DONE);
    }

    public void clear() {
        data.outPutString(Commands.ALL_PROJECTS_WILL_BE_CLEARED);
        data.outPutString(Commands.YES_NO);
        String answer = data.scanner();

        if (answer.equals(Commands.YES)) {
            projectRepository.removeAll();
            taskService.clear();
            data.outPutString(Commands.DONE);
        }
    }

    public void remove() {
        data.outPutString(Commands.ENTER_PROJECT_ID_FOR_REMOVING);
        String projectId = data.scanner();

        if (projectId != null) {
            projectRepository.remove(projectId);
            data.outPutString(Commands.DONE);
        }
    }

    public void getList() {
        Collection<Project> values = projectRepository.findAll();
        for(Project project : values){
            data.outPutProject(project);
        }
    }

    public void getOne() {
        data.outPutString(Commands.ENTER_PROJECT_ID);
        String projectId = data.scanner();
        data.outPutProject(projectRepository.findOne(projectId));
    }

    public void update() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        data.outPutString(Commands.ENTER_ID_OF_PROJECT_FOR_SHOWING);
        String projectId = data.scanner();
        if(projectRepository.findOne(projectId) != null){
            Project project = projectRepository.findOne(projectId);
            System.out.println();
            String name = data.scanner();
            System.out.println();
            String description = data.scanner();
            System.out.println();
            String startDate = data.scanner();
            System.out.println();
            String endDate = data.scanner();
            project.setName(name);
            project.setDescription(description);
            project.setStartDate(format.parse(startDate));
            project.setEndDate(format.parse(endDate));
            data.outPutString(Commands.DONE);
            projectRepository.merge(project);
        }
    }
    public Boolean checkKey(String id) {
        return projectRepository.findOne(id).getId().equals(id);
    }
}
