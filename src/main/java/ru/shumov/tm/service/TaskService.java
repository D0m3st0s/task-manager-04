package ru.shumov.tm.service;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.repository.TaskRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class TaskService {
    private TaskRepository taskRepository;
    private ProjectService projectService;
    private Data data;

    public TaskService(TaskRepository bootstrapTaskRepository, ProjectService projectService, Data data) {
        this.taskRepository = bootstrapTaskRepository;
        this.projectService = projectService;
        this.data = data;
    }

    public void create() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        data.outPutString(Commands.ENTER_PROJECT_ID_FOR_TASKS);
        String projectId = data.scanner();
        if (projectService.checkKey(projectId)) {
            data.outPutString(Commands.ENTER_TASK_NAME);
            String name = data.scanner();
            Task task = new Task();
            data.outPutString(Commands.ENTER_START_DATE_OF_TASK);
            String startDateS = data.scanner();
            Date StartDateD = format.parse(startDateS);
            data.outPutString(Commands.ENTER_DEADLINE_OF_TASK);
            String endDateS = data.scanner();
            Date endDateD = format.parse(endDateS);
            String id = UUID.randomUUID().toString();
            data.outPutString(Commands.ENTER_DESCRIPTION_OF_TASK);
            String description = data.scanner();

            task.setDescription(description);
            task.setEndDate(endDateD);
            task.setStartDate(StartDateD);
            task.setProjectId(projectId);
            task.setName(name);
            task.setId(id);

            taskRepository.persist(task);
            data.outPutString(Commands.DONE);

        } else {
            data.outPutString(Commands.PROJECT_DOES_NOT_EXIST);
        }
    }

    public void clear() {
        taskRepository.removeAll();
        data.outPutString(Commands.DONE);
    }

    public void remove() {
        data.outPutString(Commands.ENTER_TASK_ID_FOR_REMOVING);
        String taskId = data.scanner();
        if(taskId != null){
            taskRepository.remove(taskId);
            data.outPutString(Commands.DONE);
        }
    }

    public void getList() {
        Collection<Task> values = taskRepository.findAll();
        for (Task task : values){
            data.outPutTask(task);
        }
    }

    public void getOne() {
        data.outPutString(Commands.ENTER_PROJECT_ID_FOR_SHOWING_TASKS);
        String taskId = data.scanner();
        data.outPutTask(taskRepository.findOne(taskId));
    }

    public void update() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        data.outPutString(Commands.ENTER_ID_OF_TASK_FOR_SHOWING);
        String taskId = data.scanner();
        if(taskRepository.findOne(taskId) != null){
            Task task = taskRepository.findOne(taskId);
            data.outPutString(Commands.ENTER_TASK_NAME);
            String name = data.scanner();
            data.outPutString(Commands.ENTER_DESCRIPTION_OF_TASK);
            String description = data.scanner();
            data.outPutString(Commands.ENTER_START_DATE_OF_TASK);
            String startDate = data.scanner();
            data.outPutString(Commands.ENTER_DEADLINE_OF_PROJECT);
            String endDate = data.scanner();
            task.setName(name);
            task.setDescription(description);
            task.setStartDate(format.parse(startDate));
            task.setEndDate(format.parse(endDate));
            data.outPutString(Commands.DONE);
            taskRepository.merge(task);
        }
    }
}
